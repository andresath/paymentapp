package com.andresath.paymentapp.ui;

import android.app.ProgressDialog;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;

import com.andresath.paymentapp.R;

/**
 * @author Andrés Atehortúa
 */
public class BaseFragment extends Fragment {

    private ProgressDialog progressDialog;

    protected void showProgressDialog() {
        progressDialog = ProgressDialog.show(getContext(), getString(R.string.progressDialog_title),
                getString(R.string.progressDialog_message), true);
    }

    protected void hideProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    protected void showServiceErrorBanner() {
        showBanner(R.string.service_error_message);
    }

    protected void showEmptyStateBanner() {
        showBanner(R.string.empty_state_message);
    }

    private void showBanner(@StringRes int messageResId) {
        Snackbar.make(getActivity().findViewById(android.R.id.content),
                getString(messageResId), Snackbar.LENGTH_LONG).show();
    }
}
