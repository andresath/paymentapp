package com.andresath.paymentapp.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class CurrencyUtils {

    private static final String PATTERN_PESOS = "$#,##0.00;-$#,##0.00";

    public static String formatToPesos(double value) {
        final DecimalFormatSymbols dfs = DecimalFormatSymbols.getInstance(Locale.GERMANY);
        final DecimalFormat f = new DecimalFormat(PATTERN_PESOS);
        f.setDecimalFormatSymbols(dfs);
        return f.format(value);
    }
}
