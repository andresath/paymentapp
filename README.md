# PaymentApp
Ejercicio API de MercadoPago.

# Arquitectura
- Esta aplicación usa una arquitectura MVVM reactiva basada en los componentes de arquitectura de Android "Room", "ViewModel" y "LiveData".
- La comunicación con la API de MercadoPago se realiza con la ayuda de "Retrofit" y "GSON".
- Cada pantalla del flujo corresponde a un Fragmento. La data se pasa de una pantalla a otra a traves del ViewModel, que se instancia usando el Activity padre como LifeCycle Owner.
- Con el fin de solo mostrar los métodos de pago tipo tarjeta de crédito, se almacenan en DB mediante Room y se hace una query para solo obtener las tarjetas de crédito que cumplan con el monto ingresado.
- Los datos del resultado del flujo se persisten de pantalla a pantalla a traves de "SharedPreferences", que solo se pueden acceder usando el mismo ViewModel.
- La lógica para manejar las diferentes fuentes de datos se encuentra en la clase tipo Repository. Lo que permite una clara separación de responsabilidades.

