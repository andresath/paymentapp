package com.andresath.paymentapp.app.vo;

/**
 * @author Andrés Atehortúa
 */
public class PaymentFlowSelectedValues {

    private double amount;
    private String creditCardName;
    private String cardIssuerName;
    private String installmentMessage;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCreditCardName() {
        return creditCardName;
    }

    public void setCreditCardName(String creditCardName) {
        this.creditCardName = creditCardName;
    }

    public String getCardIssuerName() {
        return cardIssuerName;
    }

    public void setCardIssuerName(String cardIssuerName) {
        this.cardIssuerName = cardIssuerName;
    }

    public String getInstallmentMessage() {
        return installmentMessage;
    }

    public void setInstallmentMessage(String installmentMessage) {
        this.installmentMessage = installmentMessage;
    }
}
