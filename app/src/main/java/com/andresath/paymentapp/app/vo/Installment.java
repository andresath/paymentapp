package com.andresath.paymentapp.app.vo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Installment {

    @SerializedName("payment_method_id")
    private String paymentMethodId;
    @SerializedName("payment_type_id")
    private String paymentTypeId;
    @SerializedName("issuer")
    private CardIssuer cardIssuer;
    @SerializedName("payer_costs")
    private List<PayerCost> payerCosts;

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(String paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public CardIssuer getCardIssuer() {
        return cardIssuer;
    }

    public void setCardIssuer(CardIssuer cardIssuer) {
        this.cardIssuer = cardIssuer;
    }

    public List<PayerCost> getPayerCosts() {
        return payerCosts;
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }
}
