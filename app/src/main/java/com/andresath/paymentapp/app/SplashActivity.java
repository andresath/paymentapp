package com.andresath.paymentapp.app;

import android.os.Bundle;

import com.andresath.paymentapp.ui.BaseActivity;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new SplashFragment());
    }
}
