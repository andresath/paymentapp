package com.andresath.paymentapp.app.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.andresath.paymentapp.app.vo.PayerCost;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class InstallmentListAdapter extends RecyclerView.Adapter<InstallmentViewHolder> {

    private List<PayerCost> installmentList;
    private OnInstallmentSelectListener listener;

    public InstallmentListAdapter(OnInstallmentSelectListener listener) {
        installmentList = new ArrayList<>();
        this.listener = listener;
    }

    public void setInstallmentList(List<PayerCost> installmentList) {
        this.installmentList = installmentList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InstallmentViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InstallmentViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull InstallmentViewHolder holder, int position) {
        holder.bind(installmentList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return installmentList.size();
    }
}
