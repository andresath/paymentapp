package com.andresath.paymentapp.app;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andresath.paymentapp.vo.Resource;

/**
 * @author Andrés Atehortúa
 */
public class SplashFragment extends PaymentFlowFragment {

    private static final long DELAY_MS = 1000;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getViewModel().getPaymentMethodsRefreshResourceLiveData().observe(this, new Observer<Resource<Boolean>>() {
            @Override
            public void onChanged(@Nullable Resource<Boolean> booleanResource) {
                if (booleanResource == null) {
                    return;
                }

                switch (booleanResource.status) {
                    case SUCCESS:
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                goToMainScreen();
                            }
                        }, DELAY_MS);
                        break;
                    case ERROR:
                        showServiceErrorBanner();
                        break;
                    case EMPTY:
                        showEmptyStateBanner();
                        break;
                }
            }
        });
    }

    private void goToMainScreen() {
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getViewModel().getPaymentMethodsRefreshResourceLiveData().removeObservers(this);
    }
}
