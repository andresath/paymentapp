package com.andresath.paymentapp.app;

import android.app.Application;

import com.andresath.paymentapp.app.api.MercadoPagoApiClient;
import com.andresath.paymentapp.app.db.MercadoPagoDatabase;

public class MainApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        initDependencies();
    }

    private void initDependencies() {
        MercadoPagoApiClient.init(this);
        MercadoPagoDatabase.init(this);
    }
}
