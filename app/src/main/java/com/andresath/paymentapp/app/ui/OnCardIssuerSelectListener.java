package com.andresath.paymentapp.app.ui;

import com.andresath.paymentapp.app.vo.CardIssuer;

/**
 * @author Andrés Atehortúa
 */
public interface OnCardIssuerSelectListener {

    void onCardIssuerSelected(CardIssuer cardIssuer);
}
