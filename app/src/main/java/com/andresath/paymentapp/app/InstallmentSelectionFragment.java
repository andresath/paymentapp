package com.andresath.paymentapp.app;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andresath.paymentapp.app.ui.InstallmentListAdapter;
import com.andresath.paymentapp.app.ui.OnInstallmentSelectListener;
import com.andresath.paymentapp.app.vo.Installment;
import com.andresath.paymentapp.app.vo.PayerCost;
import com.andresath.paymentapp.vo.Resource;


/**
 * Activities that contain this fragment must implement the
 * {@link OnInstallmentSelectListener} interface
 * to handle interaction events.
 */
public class InstallmentSelectionFragment extends PaymentFlowFragment {

    //region Listener handling logic.

    private OnInstallmentSelectListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnInstallmentSelectListener) {
            listener = (OnInstallmentSelectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnInstallmentSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    //endregion

    private InstallmentListAdapter adapter;

    public InstallmentSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new InstallmentListAdapter(new OnInstallmentSelectListener() {
            @Override
            public void onInstallmentSelected(PayerCost payerCost) {
                getViewModel().setInstallmentMessage(payerCost.getRecommendedMessage());
                listener.onInstallmentSelected(payerCost);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_simple_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.installmentSelection_title);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        getViewModel().getInstallmentResourceLiveData().observe(this, new Observer<Resource<Installment>>() {
            @Override
            public void onChanged(@Nullable Resource<Installment> listResource) {
                if (listResource == null) {
                    return;
                }

                hideProgressDialog();
                switch (listResource.status) {
                    case LOADING:
                        showProgressDialog();
                        break;
                    case SUCCESS:
                        adapter.setInstallmentList(listResource.data.getPayerCosts());
                        break;
                    case ERROR:
                        showServiceErrorBanner();
                        break;
                    case EMPTY:
                        showEmptyStateBanner();
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getViewModel().getInstallmentResourceLiveData().removeObservers(this);
    }
}
