package com.andresath.paymentapp.app;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.andresath.paymentapp.app.viewmodel.MercadoPagoViewModel;
import com.andresath.paymentapp.app.vo.PaymentFlowSelectedValues;
import com.andresath.paymentapp.util.CurrencyUtils;

/**
 * @author Andrés Atehortúa
 */
public class PaymentFlowSummaryDialogFragment extends DialogFragment {

    private MercadoPagoViewModel viewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(MercadoPagoViewModel.class);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        PaymentFlowSelectedValues values = viewModel.getPaymentFlowSelectedValues();
        @SuppressLint("DefaultLocale")
        String dialogMessage = getString(R.string.summaryDialog_message_format,
                CurrencyUtils.formatToPesos(values.getAmount()), values.getCreditCardName(),
                values.getCardIssuerName(), values.getInstallmentMessage());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.summaryDialog_title);
        builder.setMessage(dialogMessage);
        builder.setPositiveButton(R.string.summaryDialog_button, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        return builder.create();
    }
}
