package com.andresath.paymentapp.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;


/**
 * Activities that contain this fragment must implement the
 * {@link OnAmountEnterListener} interface
 * to handle interaction events.
 */
public class EnterAmountFragment extends PaymentFlowFragment {

    //region Listener handling logic.

    private OnAmountEnterListener listener;

    public interface OnAmountEnterListener {

        void onAmountEntered(double amount);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnAmountEnterListener) {
            listener = (OnAmountEnterListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnAmountEnterListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    //endregion

    public EnterAmountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_amount_input, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.enterAmount_title);

        final TextInputEditText amountEditText = view.findViewById(R.id.amountEditText);
        final ImageButton sendButton = view.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amountStr = amountEditText.getText().toString();
                if (!amountStr.isEmpty()) {
                    double amount = Double.parseDouble(amountStr);
                    getViewModel().setAmount(amount);
                    listener.onAmountEntered(amount);
                } else {
                    amountEditText.setError(getString(R.string.enterAmount_error_message));
                }
            }
        });
    }
}
