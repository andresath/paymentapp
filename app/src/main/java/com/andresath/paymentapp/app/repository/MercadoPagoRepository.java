package com.andresath.paymentapp.app.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.andresath.paymentapp.app.api.MercadoPagoApiClient;
import com.andresath.paymentapp.app.cache.PaymentFlowSelectedValuesCache;
import com.andresath.paymentapp.app.db.MercadoPagoDatabase;
import com.andresath.paymentapp.app.db.PaymentMethodDao;
import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.app.vo.Installment;
import com.andresath.paymentapp.app.vo.PaymentFlowSelectedValues;
import com.andresath.paymentapp.app.vo.PaymentMethod;
import com.andresath.paymentapp.vo.Resource;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MercadoPagoRepository {

    private static MercadoPagoRepository instance;

    private final MercadoPagoApiClient apiClient;
    private final PaymentFlowSelectedValuesCache selectedValuesCache;

    private MercadoPagoRepository(@NonNull Application application) {
        apiClient = MercadoPagoApiClient.getInstance();
        selectedValuesCache = new PaymentFlowSelectedValuesCache(application);
    }

    public static MercadoPagoRepository getInstance(@NonNull Application application) {
        if (instance == null) {
            instance = new MercadoPagoRepository(application);
        }
        return instance;
    }

    public LiveData<Resource<Boolean>> refreshPaymentMethods() {
        final MutableLiveData<Resource<Boolean>> result = new MutableLiveData<>();

        result.setValue(Resource.loading(true));
        apiClient.getPaymentMethods(new Callback<List<PaymentMethod>>() {
            @Override
            public void onResponse(Call<List<PaymentMethod>> call, Response<List<PaymentMethod>> response) {
                if (response.isSuccessful()) {
                    List<PaymentMethod> paymentMethods = response.body();
                    if (paymentMethods.isEmpty()) {
                        result.setValue(Resource.<Boolean>empty());
                    } else {
                        storePaymentMethods(paymentMethods);
                        result.setValue(Resource.success(true));
                    }
                } else {
                    result.setValue(Resource.error(
                            String.format("Error Code: %s", response.code()), true));
                }
            }

            @Override
            public void onFailure(Call<List<PaymentMethod>> call, Throwable t) {
                result.setValue(Resource.error(
                        "Unknown Failure", true));
            }
        });

        return result;
    }

    private void storePaymentMethods(final List<PaymentMethod> paymentMethods) {
        final Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new Runnable() {
            @Override
            public void run() {
                final PaymentMethodDao paymentMethodDao = MercadoPagoDatabase.getInstance().paymentMethodDao();
                paymentMethodDao.deleteAll();
                paymentMethodDao.insertAll(paymentMethods);
            }
        });
    }

    public LiveData<List<PaymentMethod>> getCreditCardPaymentMethods(double amount) {
        return MercadoPagoDatabase.getInstance().paymentMethodDao().getCreditCardPaymentMethods(amount);
    }

    public LiveData<Resource<List<CardIssuer>>> getCardIssuers(@NonNull String paymentMethodId) {
        final MutableLiveData<Resource<List<CardIssuer>>> result = new MutableLiveData<>();

        result.setValue(Resource.<List<CardIssuer>>loading(null));
        apiClient.getCardIssuers(paymentMethodId, new Callback<List<CardIssuer>>() {
            @Override
            public void onResponse(Call<List<CardIssuer>> call, Response<List<CardIssuer>> response) {
                if (response.isSuccessful()) {
                    List<CardIssuer> cardIssuerList = response.body();
                    if (cardIssuerList.isEmpty()) {
                        result.setValue(Resource.<List<CardIssuer>>empty());
                    } else {
                        result.setValue(Resource.success(response.body()));
                    }
                } else {
                    result.setValue(Resource.<List<CardIssuer>>error(
                            String.format("Error Code: %s", response.code()), null));
                }
            }

            @Override
            public void onFailure(Call<List<CardIssuer>> call, Throwable t) {
                result.setValue(Resource.<List<CardIssuer>>error(
                        "Unknown Failure", null));
            }
        });

        return result;
    }

    public LiveData<Resource<Installment>> getInstallment(double amount,
                                                          @NonNull String paymentMethodId,
                                                          @NonNull String cardIssuerId) {
        final MutableLiveData<Resource<Installment>> result = new MutableLiveData<>();

        result.setValue(Resource.<Installment>loading(null));
        apiClient.getInstallment(amount, paymentMethodId, cardIssuerId, new Callback<List<Installment>>() {
            @Override
            public void onResponse(Call<List<Installment>> call, Response<List<Installment>> response) {
                if (response.isSuccessful()) {
                    Installment installment = response.body().get(0);
                    if (installment.getPayerCosts().isEmpty()) {
                        result.setValue(Resource.<Installment>empty());
                    } else {
                        result.setValue(Resource.success(installment));
                    }
                } else {
                    result.setValue(Resource.<Installment>error(
                            String.format("Error Code: %s", response.code()), null));
                }
            }

            @Override
            public void onFailure(Call<List<Installment>> call, Throwable t) {
                result.setValue(Resource.<Installment>error(
                        "Unknown Failure", null));
            }
        });

        return result;
    }

    public double loadAmountEntered() {
        return selectedValuesCache.getAmount();
    }

    public void setAmountEntered(double amount) {
        selectedValuesCache.setAmount(amount);
    }

    public String getCreditCardId() {
        return selectedValuesCache.getCreditCardId();
    }

    public void setCreditCardSelected(PaymentMethod creditCard) {
        selectedValuesCache.setCreditCard(creditCard);
    }

    public void setCardIssuerSelected(CardIssuer cardIssuer) {
        selectedValuesCache.setCardIssuer(cardIssuer);
    }

    public void setInstallmentMessage(String installmentMessage) {
        selectedValuesCache.setInstallmentMessage(installmentMessage);
    }

    public PaymentFlowSelectedValues getPaymentFlowSelectedValues() {
        return selectedValuesCache.getPaymentSelectedValues();
    }
}
