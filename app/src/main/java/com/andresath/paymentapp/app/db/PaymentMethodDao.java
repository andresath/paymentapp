package com.andresath.paymentapp.app.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.andresath.paymentapp.app.vo.PaymentMethod;

import java.util.List;

/**
 * @author Andrés Atehortúa
 */
@Dao
public interface PaymentMethodDao {

    @Insert
    void insertAll(List<PaymentMethod> brands);

    @Query("DELETE FROM PaymentMethod")
    void deleteAll();

    @Query("SELECT * FROM PaymentMethod")
    LiveData<List<PaymentMethod>> getAll();

    @Query("SELECT * FROM PaymentMethod"
            + " WHERE PaymentMethod.paymentMethod_typeId = \"credit_card\""
            + " AND PaymentMethod.paymentMethod_maxAmount >= :amount"
            + " AND PaymentMethod.paymentMethod_minAmount <= :amount")
    LiveData<List<PaymentMethod>> getCreditCardPaymentMethods(double amount);
}
