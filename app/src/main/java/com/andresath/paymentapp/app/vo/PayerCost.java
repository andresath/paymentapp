package com.andresath.paymentapp.app.vo;

import com.google.gson.annotations.SerializedName;

public class PayerCost {

    @SerializedName("installments")
    private int installments;
    @SerializedName("installment_rate")
    private double installmentRate;
    @SerializedName("installment_amount")
    private double installmentAmount;
    @SerializedName("min_allowed_amount")
    private double minAllowedAmount;
    @SerializedName("max_allowed_amount")
    private double maxAllowedAmount;
    @SerializedName("total_amount")
    private double totalAmount;
    @SerializedName("recommended_message")
    private String recommendedMessage;

    public int getInstallments() {
        return installments;
    }

    public void setInstallments(int installments) {
        this.installments = installments;
    }

    public double getInstallmentRate() {
        return installmentRate;
    }

    public void setInstallmentRate(double installmentRate) {
        this.installmentRate = installmentRate;
    }

    public double getInstallmentAmount() {
        return installmentAmount;
    }

    public void setInstallmentAmount(double installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public double getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(double minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public double getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(double maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public void setRecommendedMessage(String recommendedMessage) {
        this.recommendedMessage = recommendedMessage;
    }
}
