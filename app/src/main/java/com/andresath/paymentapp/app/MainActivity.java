package com.andresath.paymentapp.app;

import android.os.Bundle;

import com.andresath.paymentapp.app.ui.OnCardIssuerSelectListener;
import com.andresath.paymentapp.app.ui.OnInstallmentSelectListener;
import com.andresath.paymentapp.app.ui.OnPaymentMethodSelectListener;
import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.app.vo.PayerCost;
import com.andresath.paymentapp.app.vo.PaymentMethod;
import com.andresath.paymentapp.ui.BaseActivity;

public class MainActivity extends BaseActivity implements
        EnterAmountFragment.OnAmountEnterListener,
        OnPaymentMethodSelectListener,
        OnCardIssuerSelectListener,
        OnInstallmentSelectListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addFragment(new EnterAmountFragment());
    }

    @Override
    public void onAmountEntered(double amount) {
        replaceFragment(new PaymentMethodSelectionFragment());
    }

    @Override
    public void onPaymentMethodSelected(PaymentMethod paymentMethod) {
        replaceFragment(new CardIssuerSelectionFragment());
    }

    @Override
    public void onCardIssuerSelected(CardIssuer cardIssuer) {
        replaceFragment(new InstallmentSelectionFragment());
    }

    @Override
    public void onInstallmentSelected(PayerCost payerCost) {
        clearBackStack();
        addFragment(new EnterAmountFragment());

        PaymentFlowSummaryDialogFragment summaryDialogFragment = new PaymentFlowSummaryDialogFragment();
        summaryDialogFragment.show(getSupportFragmentManager(), "paymentFlowSummary");
    }
}
