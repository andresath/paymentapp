package com.andresath.paymentapp.app;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andresath.paymentapp.app.ui.CardIssuerListAdapter;
import com.andresath.paymentapp.app.ui.OnCardIssuerSelectListener;
import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.vo.Resource;

import java.util.List;


/**
 * Activities that contain this fragment must implement the
 * {@link OnCardIssuerSelectListener} interface
 * to handle interaction events.
 */
public class CardIssuerSelectionFragment extends PaymentFlowFragment {

    //region Listener handling logic.

    private OnCardIssuerSelectListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCardIssuerSelectListener) {
            listener = (OnCardIssuerSelectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCardIssuerSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    //endregion

    private CardIssuerListAdapter adapter;

    public CardIssuerSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new CardIssuerListAdapter(new OnCardIssuerSelectListener() {
            @Override
            public void onCardIssuerSelected(CardIssuer cardIssuer) {
                getViewModel().setCardIssuer(cardIssuer);
                listener.onCardIssuerSelected(cardIssuer);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_simple_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.cardIssuerSelection_title);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        getViewModel().getCardIssuersResourceLiveData().observe(this, new Observer<Resource<List<CardIssuer>>>() {
            @Override
            public void onChanged(@Nullable Resource<List<CardIssuer>> listResource) {
                if (listResource == null) {
                    return;
                }

                hideProgressDialog();
                switch (listResource.status) {
                    case LOADING:
                        showProgressDialog();
                        break;
                    case SUCCESS:
                        adapter.setCardIssuerList(listResource.data);
                        break;
                    case ERROR:
                        showServiceErrorBanner();
                        break;
                    case EMPTY:
                        showEmptyStateBanner();
                        break;
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getViewModel().getCardIssuersResourceLiveData().removeObservers(this);
    }
}
