package com.andresath.paymentapp.app.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.andresath.paymentapp.app.vo.PaymentMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class PaymentMethodListAdapter extends RecyclerView.Adapter<PaymentMethodViewHolder> {

    private List<PaymentMethod> paymentMethodList;
    private OnPaymentMethodSelectListener listener;

    public PaymentMethodListAdapter(OnPaymentMethodSelectListener listener) {
        paymentMethodList = new ArrayList<>();
        this.listener = listener;
    }

    public void setPaymentMethodList(List<PaymentMethod> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public PaymentMethodViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new PaymentMethodViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentMethodViewHolder holder, int position) {
        holder.bind(paymentMethodList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return paymentMethodList.size();
    }
}
