package com.andresath.paymentapp.app.cache;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.app.vo.PaymentFlowSelectedValues;
import com.andresath.paymentapp.app.vo.PaymentMethod;

/**
 * @author Andrés Atehortúa
 */
public class PaymentFlowSelectedValuesCache {

    private static final String PREFS_FILE_NAME = "paymentFlowSelectedValues";
    private static final String PREFS_KEY_AMOUNT = "amount";
    private static final String PREFS_KEY_CREDIT_CARD_ID = "creditCardId";
    private static final String PREFS_KEY_CREDIT_CARD_NAME = "creditCardName";
    private static final String PREFS_KEY_CARD_ISSUER_ID = "cardIssuerId";
    private static final String PREFS_KEY_CARD_ISSUER_NAME = "cardIssuerName";
    private static final String PREFS_KEY_INSTALLMENT_MESSAGE = "installmentMessage";

    private final SharedPreferences prefs;

    public PaymentFlowSelectedValuesCache(Application application) {
        prefs = application.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    public double getAmount() {
        return Double.parseDouble(prefs.getString(PREFS_KEY_AMOUNT, "0"));
    }

    public void setAmount(double amount) {
        prefs.edit().putString(PREFS_KEY_AMOUNT, String.valueOf(amount)).apply();
    }

    public String getCreditCardId() {
        return prefs.getString(PREFS_KEY_CREDIT_CARD_ID, "visa");
    }

    public String getCreditCardName() {
        return prefs.getString(PREFS_KEY_CREDIT_CARD_NAME, "");
    }

    public void setCreditCard(PaymentMethod creditCard) {
        prefs.edit()
                .putString(PREFS_KEY_CREDIT_CARD_ID, creditCard.getId())
                .putString(PREFS_KEY_CREDIT_CARD_NAME, creditCard.getName())
                .apply();
    }

    public String getCardIssuerId() {
        return prefs.getString(PREFS_KEY_CARD_ISSUER_ID, "1005");
    }

    public String getCardIssuerName() {
        return prefs.getString(PREFS_KEY_CARD_ISSUER_NAME, "");
    }

    public void setCardIssuer(CardIssuer cardIssuer) {
        prefs.edit()
                .putString(PREFS_KEY_CARD_ISSUER_ID, cardIssuer.getId())
                .putString(PREFS_KEY_CARD_ISSUER_NAME, cardIssuer.getName())
                .apply();
    }

    public String getInstallmentMessage() {
        return prefs.getString(PREFS_KEY_INSTALLMENT_MESSAGE, "");
    }

    public void setInstallmentMessage(String installmentMessage) {
        prefs.edit().putString(PREFS_KEY_INSTALLMENT_MESSAGE, installmentMessage).apply();
    }

    public PaymentFlowSelectedValues getPaymentSelectedValues() {
        PaymentFlowSelectedValues values = new PaymentFlowSelectedValues();
        values.setAmount(getAmount());
        values.setCreditCardName(getCreditCardName());
        values.setCardIssuerName(getCardIssuerName());
        values.setInstallmentMessage(getInstallmentMessage());
        return values;
    }
}
