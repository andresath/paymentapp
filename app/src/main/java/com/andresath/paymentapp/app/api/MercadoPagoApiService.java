package com.andresath.paymentapp.app.api;

import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.app.vo.Installment;
import com.andresath.paymentapp.app.vo.PaymentMethod;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MercadoPagoApiService {

    String PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";

    @GET("/v1/payment_methods")
    Call<List<PaymentMethod>> getPaymentMethods(@Query("public_key") String publicKey);

    @GET("/v1/payment_methods/card_issuers")
    Call<List<CardIssuer>> getCardIssuers(@Query("public_key") String publicKey,
                                          @Query("payment_method_id") String paymentMethodId);

    @GET("/v1/payment_methods/installments")
    Call<List<Installment>> getInstallment(@Query("public_key") String publicKey,
                                     @Query("amount") double amount,
                                     @Query("issuer.id") String cardIssuerId,
                                     @Query("payment_method_id") String paymentMethodId);
}
