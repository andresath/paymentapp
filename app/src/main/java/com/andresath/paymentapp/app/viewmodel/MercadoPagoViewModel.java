package com.andresath.paymentapp.app.viewmodel;

import android.app.Application;
import android.arch.core.util.Function;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.andresath.paymentapp.app.repository.MercadoPagoRepository;
import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.app.vo.Installment;
import com.andresath.paymentapp.app.vo.PaymentFlowSelectedValues;
import com.andresath.paymentapp.app.vo.PaymentMethod;
import com.andresath.paymentapp.vo.Resource;

import java.util.List;

public class MercadoPagoViewModel extends AndroidViewModel {

    private MercadoPagoRepository repository;
    private LiveData<Resource<Boolean>> paymentMethodsRefreshResourceLiveData;
    private MutableLiveData<Double> amountEntered;
    private LiveData<List<PaymentMethod>> creditCardsResourceLiveData;
    private MutableLiveData<PaymentMethod> creditCardSelected;
    private LiveData<Resource<List<CardIssuer>>> cardIssuersResourceLiveData;
    private MutableLiveData<CardIssuer> cardIssuerSelected;
    private LiveData<Resource<Installment>> installmentResourceLiveData;


    public MercadoPagoViewModel(@NonNull Application application) {
        super(application);

        repository = MercadoPagoRepository.getInstance(application);

        amountEntered = new MutableLiveData<>();
        creditCardsResourceLiveData = Transformations.switchMap(amountEntered, new Function<Double, LiveData<List<PaymentMethod>>>() {
            @Override
            public LiveData<List<PaymentMethod>> apply(Double amount) {
                return repository.getCreditCardPaymentMethods(amount);
            }
        });
        creditCardSelected = new MutableLiveData<>();
        cardIssuersResourceLiveData = Transformations.switchMap(creditCardSelected, new Function<PaymentMethod, LiveData<Resource<List<CardIssuer>>>>() {
            @Override
            public LiveData<Resource<List<CardIssuer>>> apply(PaymentMethod creditCard) {
                return repository.getCardIssuers(creditCard.getId());
            }
        });
        cardIssuerSelected = new MutableLiveData<>();
        installmentResourceLiveData = Transformations.switchMap(cardIssuerSelected, new Function<CardIssuer, LiveData<Resource<Installment>>>() {
            @Override
            public LiveData<Resource<Installment>> apply(CardIssuer input) {
                final double amount = repository.loadAmountEntered();
                final String creditCardId = repository.getCreditCardId();
                return repository.getInstallment(amount, creditCardId, input.getId());
            }
        });
    }

    public LiveData<Resource<Boolean>> getPaymentMethodsRefreshResourceLiveData() {
        if (paymentMethodsRefreshResourceLiveData == null) {
            paymentMethodsRefreshResourceLiveData = repository.refreshPaymentMethods();
        }
        return paymentMethodsRefreshResourceLiveData;
    }

    public void setAmount(double amount) {
        repository.setAmountEntered(amount);
        amountEntered.setValue(amount);
    }

    public LiveData<List<PaymentMethod>> getCreditCardsResourceLiveData() {
        return creditCardsResourceLiveData;
    }

    public void setCreditCard(PaymentMethod creditCard) {
        repository.setCreditCardSelected(creditCard);
        creditCardSelected.setValue(creditCard);
    }

    public LiveData<Resource<List<CardIssuer>>> getCardIssuersResourceLiveData() {
        return cardIssuersResourceLiveData;
    }

    public void setCardIssuer(CardIssuer cardIssuer) {
        repository.setCardIssuerSelected(cardIssuer);
        cardIssuerSelected.setValue(cardIssuer);
    }

    public LiveData<Resource<Installment>> getInstallmentResourceLiveData() {
        return installmentResourceLiveData;
    }

    public void setInstallmentMessage(String installmentMessage) {
        repository.setInstallmentMessage(installmentMessage);
    }

    public PaymentFlowSelectedValues getPaymentFlowSelectedValues() {
        return repository.getPaymentFlowSelectedValues();
    }
}
