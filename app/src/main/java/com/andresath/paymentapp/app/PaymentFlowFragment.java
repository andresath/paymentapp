package com.andresath.paymentapp.app;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.andresath.paymentapp.app.viewmodel.MercadoPagoViewModel;
import com.andresath.paymentapp.ui.BaseFragment;

/**
 * @author Andrés Atehortúa
 */
public class PaymentFlowFragment extends BaseFragment {

    private MercadoPagoViewModel viewModel;

    protected MercadoPagoViewModel getViewModel() {
        return viewModel;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        viewModel = ViewModelProviders.of(getActivity()).get(MercadoPagoViewModel.class);
    }
}
