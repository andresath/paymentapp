package com.andresath.paymentapp.app.api;

import android.app.Application;
import android.content.Context;
import android.support.annotation.NonNull;

import com.andresath.paymentapp.app.vo.CardIssuer;
import com.andresath.paymentapp.app.vo.Installment;
import com.andresath.paymentapp.app.vo.PaymentMethod;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MercadoPagoApiClient {

    private static MercadoPagoApiClient instance;

    private final MercadoPagoApiService service;

    private MercadoPagoApiClient(Context context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.mercadopago.com")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(MercadoPagoApiService.class);
    }

    public static void init(Application application) {
        instance = new MercadoPagoApiClient(application);
    }

    public static MercadoPagoApiClient getInstance() {
        if (instance == null) {
            throw new NullPointerException("Did you forget to call init?");
        }
        return instance;
    }

    public void getPaymentMethods(Callback<List<PaymentMethod>> callback) {
        service.getPaymentMethods(MercadoPagoApiService.PUBLIC_KEY).enqueue(callback);
    }

    public void getCardIssuers(@NonNull String paymentMethodId, Callback<List<CardIssuer>> callback) {
        service.getCardIssuers(MercadoPagoApiService.PUBLIC_KEY, paymentMethodId).enqueue(callback);
    }

    public void getInstallment(double amount, @NonNull String paymentMethodId,
                               @NonNull String cardIssuerId, Callback<List<Installment>> callback) {
        service.getInstallment(MercadoPagoApiService.PUBLIC_KEY, amount, cardIssuerId, paymentMethodId)
                .enqueue(callback);
    }
}
