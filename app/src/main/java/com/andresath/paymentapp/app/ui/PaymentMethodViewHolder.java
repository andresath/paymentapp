package com.andresath.paymentapp.app.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andresath.paymentapp.app.R;
import com.andresath.paymentapp.app.vo.PaymentMethod;
import com.bumptech.glide.Glide;

/**
 * @author Andrés Atehortúa
 */
public class PaymentMethodViewHolder extends RecyclerView.ViewHolder {

    private TextView nameTextView;
    private ImageView iconImageView;

    public PaymentMethodViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_payment_method, parent, false));

        nameTextView = itemView.findViewById(R.id.nameTextView);
        iconImageView = itemView.findViewById(R.id.iconImageView);
    }

    public void bind(final PaymentMethod paymentMethod,
                     final OnPaymentMethodSelectListener listener) {
        nameTextView.setText(paymentMethod.getName());
        Glide.with(itemView)
                .load(paymentMethod.getThumbnailUrl())
                .into(iconImageView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onPaymentMethodSelected(paymentMethod);
                }
            }
        });
    }
}
