package com.andresath.paymentapp.app.ui;

import com.andresath.paymentapp.app.vo.PayerCost;

/**
 * @author Andrés Atehortúa
 */
public interface OnInstallmentSelectListener {

    void onInstallmentSelected(PayerCost payerCost);
}
