package com.andresath.paymentapp.app.ui;

import com.andresath.paymentapp.app.vo.PaymentMethod;

/**
 * @author Andrés Atehortúa
 */
public interface OnPaymentMethodSelectListener {

    void onPaymentMethodSelected(PaymentMethod paymentMethod);
}
