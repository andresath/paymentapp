package com.andresath.paymentapp.app.vo;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

@Entity
public class PaymentMethod {

    @SerializedName("id")
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "paymentMethod_id")
    private String id;
    @SerializedName("name")
    @ColumnInfo(name = "paymentMethod_name")
    private String name;
    @SerializedName("payment_type_id")
    @ColumnInfo(name = "paymentMethod_typeId")
    private String paymentTypeId;
    @SerializedName("thumbnail")
    @ColumnInfo(name = "paymentMethod_thumbnailUrl")
    private String thumbnailUrl;
    @SerializedName("min_allowed_amount")
    @ColumnInfo(name = "paymentMethod_minAmount")
    private double minAllowedAmount;
    @SerializedName("max_allowed_amount")
    @ColumnInfo(name = "paymentMethod_maxAmount")
    private double maxAllowedAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public double getMinAllowedAmount() {
        return minAllowedAmount;
    }

    public void setMinAllowedAmount(double minAllowedAmount) {
        this.minAllowedAmount = minAllowedAmount;
    }

    public double getMaxAllowedAmount() {
        return maxAllowedAmount;
    }

    public void setMaxAllowedAmount(double maxAllowedAmount) {
        this.maxAllowedAmount = maxAllowedAmount;
    }
}
