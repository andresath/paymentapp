package com.andresath.paymentapp.app.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andresath.paymentapp.app.R;
import com.andresath.paymentapp.app.vo.PayerCost;

/**
 * @author Andrés Atehortúa
 */
public class InstallmentViewHolder extends RecyclerView.ViewHolder {

    private TextView messageTextView;

    public InstallmentViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_installment, parent, false));

        messageTextView = itemView.findViewById(R.id.messageTextView);
    }

    public void bind(final PayerCost payerCost,
                     final OnInstallmentSelectListener listener) {
        messageTextView.setText(payerCost.getRecommendedMessage());

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onInstallmentSelected(payerCost);
                }
            }
        });
    }
}
