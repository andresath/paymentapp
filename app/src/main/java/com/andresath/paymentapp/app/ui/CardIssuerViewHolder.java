package com.andresath.paymentapp.app.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.andresath.paymentapp.app.R;
import com.andresath.paymentapp.app.vo.CardIssuer;
import com.bumptech.glide.Glide;

/**
 * @author Andrés Atehortúa
 */
public class CardIssuerViewHolder extends RecyclerView.ViewHolder {

    private TextView nameTextView;
    private ImageView iconImageView;

    public CardIssuerViewHolder(ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_card_issuer, parent, false));

        nameTextView = itemView.findViewById(R.id.nameTextView);
        iconImageView = itemView.findViewById(R.id.iconImageView);
    }

    public void bind(final CardIssuer cardIssuer,
                     final OnCardIssuerSelectListener listener) {
        nameTextView.setText(cardIssuer.getName());
        Glide.with(itemView)
                .load(cardIssuer.getThumbnailUrl())
                .into(iconImageView);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onCardIssuerSelected(cardIssuer);
                }
            }
        });
    }
}
