package com.andresath.paymentapp.app.ui;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.andresath.paymentapp.app.vo.CardIssuer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Andrés Atehortúa
 */
public class CardIssuerListAdapter extends RecyclerView.Adapter<CardIssuerViewHolder> {

    private List<CardIssuer> cardIssuerList;
    private OnCardIssuerSelectListener listener;

    public CardIssuerListAdapter(OnCardIssuerSelectListener listener) {
        cardIssuerList = new ArrayList<>();
        this.listener = listener;
    }

    public void setCardIssuerList(List<CardIssuer> cardIssuerList) {
        this.cardIssuerList = cardIssuerList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CardIssuerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CardIssuerViewHolder(parent);
    }

    @Override
    public void onBindViewHolder(@NonNull CardIssuerViewHolder holder, int position) {
        holder.bind(cardIssuerList.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return cardIssuerList.size();
    }
}
