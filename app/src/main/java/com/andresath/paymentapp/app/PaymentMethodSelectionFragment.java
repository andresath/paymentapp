package com.andresath.paymentapp.app;

import android.arch.lifecycle.Observer;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andresath.paymentapp.app.ui.OnPaymentMethodSelectListener;
import com.andresath.paymentapp.app.ui.PaymentMethodListAdapter;
import com.andresath.paymentapp.app.vo.PaymentMethod;

import java.util.List;


/**
 * Activities that contain this fragment must implement the
 * {@link com.andresath.paymentapp.app.ui.OnPaymentMethodSelectListener} interface
 * to handle interaction events.
 */
public class PaymentMethodSelectionFragment extends PaymentFlowFragment {

    //region Listener handling logic.

    private OnPaymentMethodSelectListener listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPaymentMethodSelectListener) {
            listener = (OnPaymentMethodSelectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnPaymentMethodSelectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    //endregion

    private PaymentMethodListAdapter adapter;

    public PaymentMethodSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new PaymentMethodListAdapter(new OnPaymentMethodSelectListener() {
            @Override
            public void onPaymentMethodSelected(PaymentMethod paymentMethod) {
                getViewModel().setCreditCard(paymentMethod);
                listener.onPaymentMethodSelected(paymentMethod);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_simple_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Toolbar toolbar = view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.paymentMethodSelection_title);
//        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);

        getViewModel().getCreditCardsResourceLiveData().observe(this, new Observer<List<PaymentMethod>>() {
            @Override
            public void onChanged(@Nullable List<PaymentMethod> creditCards) {
                if (creditCards == null) {
                    return;
                }

                adapter.setPaymentMethodList(creditCards);
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        getViewModel().getCreditCardsResourceLiveData().removeObservers(this);
    }
}
