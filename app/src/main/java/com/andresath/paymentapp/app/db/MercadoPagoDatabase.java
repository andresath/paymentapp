package com.andresath.paymentapp.app.db;

import android.app.Application;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;

import com.andresath.paymentapp.app.vo.PaymentMethod;

@Database(
        entities = {
                PaymentMethod.class
        }, version = 1, exportSchema = false)
public abstract class MercadoPagoDatabase extends RoomDatabase {

    private static final String DB_NAME = "mercadopago_db";

    private static MercadoPagoDatabase instance;

    public static void init(Application application) {
        instance = Room.databaseBuilder(application, MercadoPagoDatabase.class, DB_NAME).build();
    }

    public static MercadoPagoDatabase getInstance() {
        if (instance == null) {
            throw new NullPointerException("Did you forget to call init?");
        }
        return instance;
    }

    public abstract PaymentMethodDao paymentMethodDao();
}
